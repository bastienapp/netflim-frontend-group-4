import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import HomePage from './pages/Home/HomePage'
import Account from './pages/Account/Account'
import DetailMovie from './pages/Details/DetailMovie'
import ToWatch from './pages/Lists/ToWatch'
import Contact from './pages/Contact/Contact'
import Legals from "./pages/Legals/Legals"
import Policy from "./pages/Policy/Policy"

import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom";
import SeenList from "./pages/Lists/SeenList";
import FavouriteList from "./pages/Lists/FavouriteList";


const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/account",
    element: <Account />,
  },
  {
    path: "/towatchlist",
    element: <ToWatch />,
  },
  {
    path: "/towatchedlist",
    element: <SeenList />,
  },
  {
    path: "/tofavouritelist",
    element: <FavouriteList />,
  },
  {
    // déclare la route et prend un paramètre movieId automatiquement (qui sera récupéré par useParams)
    path: "/detail/:id",
    element: <DetailMovie />,
  },
  {
    path: "/contact",
    element: <Contact />,
  },
  {
    path: "/legals",
    element: <Legals />,
  },
  {
    path: "/policy",
    element: <Policy />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
