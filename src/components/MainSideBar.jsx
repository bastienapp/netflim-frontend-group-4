import { Link } from "react-router-dom";
import "../components/MainSidebar.css";

export default function MainSideBar({
  sortPopular,
  sortByReleaseDate,
  sortByDuration,
  sortByRate,
  filterByCategory,
}) {
  return (
    <div className="sidebar__content">
      <nav>
        <h1>Mes listes </h1>
        <ul>
          <li>
            <Link to={`/`}>Accueil</Link>
          </li>
          <li>
            <Link to={`/account`}>Votre compte</Link>
          </li>
          <li>
            <Link to={`/towatchlist`}>Liste à voir</Link>
          </li>
          <li>
            <Link to={`/towatchedlist`}>Liste vus</Link>
          </li>
          <li>
            <Link to={`/tofavouritelist`}>Liste préférés</Link>
          </li>
        </ul>
      </nav>
      <nav>
        <h1>Trier par</h1>
        <ul>
          <li>
            <button onClick={sortPopular}>Popularité</button>
          </li>
          <li>
            <button onClick={(e) => sortByReleaseDate("Les plus récents")}>
              Plus récent
            </button>
          </li>
          <li>
            <button onClick={(e) => sortByReleaseDate("Les plus anciens")}>
              Plus ancien
            </button>
          </li>
          <li>
            <button onClick={(e) => sortByDuration("Les plus longs")}>
              Durée (long à court)
            </button>
          </li>
          <li>
            <button onClick={(e) => sortByDuration("Les plus courts")}>
              Durée (court à long)
            </button>
          </li>

          <li>
            <button onClick={(e) => sortByRate("Les mieux notés")}>
              Meilleures notes
            </button>
          </li>
          <li>
            <button onClick={(e) => sortByRate("Les moins bien notés")}>Plus basses notes</button>
          </li>
        </ul>
      </nav>
      <nav>
        <h1>Catégories</h1>
        <ul>
          <li><button onClick={(e) => filterByCategory("Comedy")}>Comédie</button></li>
          <li><button onClick={(e) => filterByCategory("Horror")}>Horreur</button></li>
          <li><button onClick={(e) => filterByCategory("Thriller")}>Thriller</button></li>
          <li><button onClick={(e) => filterByCategory("Romance")}>Romance</button></li>
          <li><button onClick={(e) => filterByCategory("Crime")}>Crime</button></li>
          <li><button onClick={(e) => filterByCategory("History")}>Histoire</button></li>
          <li><button onClick={(e) => filterByCategory("Science Fiction")}>Science fiction</button></li>
          <li><button onClick={(e) => filterByCategory("Fantasy")}>Fantastique</button></li>
          <li><button onClick={(e) => filterByCategory("Action")}>Action</button></li>
          <li><button onClick={(e) => filterByCategory("Drama")}>Dramatique</button></li>
          <li><button onClick={(e) => filterByCategory("Family")}>Famille</button></li>
          <li><button onClick={(e) => filterByCategory("Music")}>Musique</button></li>
          <li><button onClick={(e) => filterByCategory("Animation")}>Animation</button></li>
          <li><button onClick={(e) => filterByCategory("Adventure")}>Aventure</button></li>
          <li><button onClick={(e) => filterByCategory("Mystery")}>Mystère</button></li>
          <li><button onClick={(e) => filterByCategory("Western")}>Western</button></li>
          <li><button onClick={(e) => filterByCategory("All")}>Tous</button></li>
        </ul>
      </nav>
    </div>
  );
}
