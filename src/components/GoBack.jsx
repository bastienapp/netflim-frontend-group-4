import React, {useState} from "react";
import goBack from "../assets/goBack.svg";
import goBackOrange from "../assets/goBackOrange.svg";
import "./GoBack.css";
import {Link} from "react-router-dom";

export default function GoBack() {
  // État pour gérer l'effet de survol sur la flèche de retour (composant GoBack)
  // On passe par useState car on a besoin de changer aussi l'image de retour. Si c'était que la couleur de "Retour", du css aurait suffit.
  const [isGoBackHovered, setIsGoBackHovered] = useState(false);

  return (
    <Link
      to="/"
      className="go-back"
      // Gérer le survol du composant. On chage l'état Hovered si le retour est en hover.
      onMouseEnter={() => setIsGoBackHovered(true)} // Lorsque la souris entre dans la zone du composant (<a> dans ce cas)
      onMouseLeave={() => setIsGoBackHovered(false)} // Lorsque la souris quitte la zone du composant>Back to Pokémon list
    >
      {/* Condition pour afficher le texte et l'image appropriés en fonction du survol */}
      {isGoBackHovered ? (
        <>
          {/* Texte "Retour" avec une couleur orange lorsqu'il est survolé. Le <span> permet de cibler spécifiquement le texte "Retour" lorsqu'il est survolé, en lui appliquant un style différent.*/}
          <span style={{color: "#F0BE3B"}}>Retour </span>
          {/* Image de la flèche de retour orange */}
          <img
            src={goBackOrange}
            alt="Flèche de retour"
            className="detail-movie-logo-back"
          />
        </>
      ) : (
        <>
          {/* Texte "Retour" en noir (couleur de base) lorsqu'il n'est pas survolé */}
          Retour
          {/* Image de la flèche de retour en noir (de base)*/}
          <img
            src={goBack}
            alt="Flèche de retour"
            className="detail-movie-logo-back"
          />
        </>
      )}
    </Link>
  );
}
