import React from "react";
import logoHeart from "../assets/heart.svg";
import logoMark from "../assets/mark.svg";
import logoEyeWhite from "../assets/logoEyeWhite.svg";
import "./MovieCard.css";
import ToggleMovieButton from "./ToggleMovieButton";
import {useLocalStorage} from "./UseLocalStorage";
import {Link} from "react-router-dom";

// URL de base pour les images de film provenant de l'API
const beginningUrl = "https://image.tmdb.org/t/p/w500";

export default function MovieCard(props) {
  const {title, poster, id, duration} = props;

  // Utilisation du hook useLocalStorage pour gérer l'état local d'ajout à la liste à regarder
  const [addToWatch, setAddToWatch] = useLocalStorage(`to-watch ${id}`, false);
  const [addToWatched, setAddToWatched] = useLocalStorage(`watched ${id}`, false);
  const [addToFavourites, setAddToFavourites] = useLocalStorage(
    `favourites ${id}`,
    false
  );

  // Fonction de basculement pour changer l'état d'ajout à la liste à regarder
  function toggleToWatch() {
    // Changement de l'état de addToWatch (vrai ou faux)
    setAddToWatch(!addToWatch);
  }

  // Fonction de basculement pour changer l'état d'ajout à la liste vus
  function toggleToWatched() {
    // Changement de l'état de addWatched (vrai ou faux)
    setAddToWatched(!addToWatched);
  }

  // Fonction de basculement pour changer l'état d'ajout à la liste préférés
  function toggleToFavourites() {
    // Changement de l'état de addToFavourites (vrai ou faux)
    setAddToFavourites(!addToFavourites);
  }

  return (
    <article className="movie-card" data-title={title}>
      {/* Utilisation du composant de navigation Link pour rediriger vers la page de détail du film */}
      <Link className="home-link-moviecard" to={`/detail/${id}`}>
        {/* <div className="image-container"> */}
        {/* </div> */}
          <img
            className="movie-card-image"
            src={`${beginningUrl}/${poster}`}
            alt={title}
            />
            {/* <p className="movie__duration">{moreDetails()}</p> */}
          <div className="overlay">
            {/* Overlay pour d'éventuels éléments interactifs supplémentaires */}
            <p className="movie__duration">{`Durée : ${duration} min`}</p>
          </div>

        {/* Paragraphe du titre du film avec gestion de la longueur du titre */}
        <p className="title-movie-card">
          {title.length > 20 ? title.substring(0, 40) + " [...]" : title}
        </p>
      </Link>

      {/* Conteneur des logos interactifs (ajout à la liste "à voir") */}
      <div className="interactive-logos">
        <ToggleMovieButton
          src={logoHeart}
          alt="Liste préférés"
          action={toggleToFavourites}
          addToFavourites={addToFavourites}
        />
        <ToggleMovieButton
          src={logoEyeWhite}
          alt="Liste vus"
          action={toggleToWatched}
          addToWatched={addToWatched}
        />
        <ToggleMovieButton
          src={logoMark}
          alt="Liste à voir"
          action={toggleToWatch}
          addToWatch={addToWatch}
        />
      </div>
    </article>
  );
}
