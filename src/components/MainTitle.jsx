import React from 'react'

function MainTitle({mainTitle}) {
  return (
    <div>{mainTitle}</div>
  )
}

export default MainTitle