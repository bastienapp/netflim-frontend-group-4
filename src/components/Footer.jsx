import "./Footer.css";
import netflimLogo from "../assets/logo-netflim.svg";
import {Link} from "react-router-dom";

function Footer() {
  return (
    <div className="footer">
      <Link to={`/`}>
        <img className="footer-logo" src={netflimLogo} alt="Logo" />
      </Link>
      <Link to={`/contact`} className="linkFooter">
        Contact
      </Link>
      <Link to={`/legals`} className="linkFooter">
        Mentions légales
      </Link>
      <Link to={`/policy`} className="linkFooter">
        Politique de cookies
      </Link>
      <p>© 2023 Netflim</p>
    </div>
  );
}

export default Footer;
