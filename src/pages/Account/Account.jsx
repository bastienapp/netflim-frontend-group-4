import SideBar from "./SideBar";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import layers from "../../assets/layers.svg";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import GoBack from "../../components/GoBack";
import "./Account.css";
import LastToWatch from "./LastToWatch";
import LastToWatched from "./LastToWatched";
import LastToFavourite from "./LastToFavourite";

import {useState} from "react";

export default function Account() {
  // Utilisation de l'état local pour stocker le nombre total de films dans la liste "à voir" et une fonction pour le mettre à jour
  const [totalMoviesInToWatch, setTotalMoviesInToWatch] = useState(0);
  const [totalMoviesInToWatched, setTotalMoviesInToWatched] = useState(0);
  const [totalMoviesInToFavourite, setTotalMoviesInToFavourite] = useState(0);

  return (
    <div className="account-wrapper">
      <header className="account-header">
        <Header />
      </header>

      {/* Section du contenu principal */}
      <div className="account-content-wrapper">
        {/* Sidebar pour la navigation */}
        <div className="account-content-sidebar">
          <SideBar />
        </div>

        {/* Section principale du contenu */}
        <div className="account-content-main">
          <div className="account-content-main-logos-top">
            <GoBack />
            <img src={layers} alt="Logo de couches" />
          </div>

          {/* Section pour les titres */}
          <div className="account-titles-container">
            <h1>
              Bienvenue <strong>Jean Michel</strong> <br /> une petite comédie
              ce soir ?{" "}
            </h1>

            {totalMoviesInToWatch !== 0 ? (
              totalMoviesInToWatch === 1 ? (
                <h2>
                  Vous avez <strong>{totalMoviesInToWatch} film</strong> dans
                  votre liste "à voir" :)
                </h2>
              ) : (
                <h2>
                  Vous avez <strong>{totalMoviesInToWatch} films</strong> dans
                  votre liste "à voir" :)
                </h2>
              )
            ) : (
              <h2>
                Vous devriez faire un tour sur l'accueil pour ajouter des films
                à voir !
              </h2>
            )}

            {/* On insère le nombre de films de la liste à voir */}
          </div>

          {/* Section pour chaque liste individuelle */}
          <div className="account-individual-list-container">
            {/* Appel du composant LastToWatch avec les propriétés nécessaires */}
            <h2 className="account-last-to-title">À voir</h2>
            <LastToWatch
              totalMoviesInToWatch={totalMoviesInToWatch}
              setTotalMoviesInToWatch={setTotalMoviesInToWatch}
            />
          </div>

          <div className="account-individual-list-container">
            <h2 className="account-last-to-title">Vus</h2>

            <LastToWatched
              totalMoviesInToWatched={totalMoviesInToWatched}
              setTotalMoviesInToWatched={setTotalMoviesInToWatched}
            />
          </div>

          {/* Pour simuler d'autres listes à la suite */}
          <div className="account-individual-list-container">
            <h2 className="account-last-to-title">Préférés</h2>
            <LastToFavourite
              totalMoviesInToFavourite={totalMoviesInToFavourite}
              setTotalMoviesInToFavourite={setTotalMoviesInToFavourite}
            />
          </div>

          <ButtonMoveToTop />
        </div>
      </div>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}
