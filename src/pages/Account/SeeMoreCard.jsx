// Importation de React et de la feuille de style dédiée à SeeMoreCard
import React from "react";
import "./SeeMoreCard.css";
import {Link} from "react-router-dom"; // Importation du composant de navigation Link

// Définition du composant fonctionnel SeeMoreCard
export default function SeeMoreCard(props) {
  // Extraction de la propriété 'number' depuis les propriétés du composant parent
  const {number, url} = props;

  // Retourne la structure JSX du composant SeeMoreCard
  return (
    <article className="see-more-card">
      {/* Utilisation du composant de navigation Link pour rediriger vers la liste à voir */}
      <Link to={url} className="image-container">
        <div className="see-more-card-content">
          <p className="see-more-card-circle">
            {number > 99 ? `+ 99` : `${number}`} <br /> films {/* On affiche +99 quand le nombre de films dan sla liste est supérieur à 99*/}
          </p>
          <p className="see-more-card-text">Voir la liste entière</p>
        </div>
      </Link>
    </article>
  );
}
