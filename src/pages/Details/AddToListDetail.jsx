import React, {useState} from "react";
import "./AddToListDetail.css";

export default function AddToListDetail(props) {
  // On récupère l'image en noire, l'image version orange, le texte à afficher, le titre pour le Alt de l'image, et le onClick qui contient une fonction comme handleAddToToWatch pour gérer l'état true ou false dans une liste (maj du localStorage)
  const {image, imageAlt, caption, title, onClick} = props;

  // Pour gérer l'effet de survol sur le composant
  const [isHovered, setIsHovered] = useState(false);

  // Fonction pour gérer le survol du composant (entrée de la souris)
  const handleMouseEnter = () => {
    setIsHovered(true); //Sert juste à mettre true dans le useState quand la mouse est en hover, pour ensuite changer le logo en version orange et changer la caption du texte
  };

  // Fonction pour gérer la sortie de la souris du composant
  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  return (
    <div
      className="add-to-list"
      onMouseEnter={handleMouseEnter} // Gère l'événement de survol (entrée de la souris)
      onMouseLeave={handleMouseLeave} // Gère l'événement de sortie de la souris
      onClick={onClick} // Gère l'événement de clic
      // En mettant ces gestionnaires d'événements dans la balise div, on spécifie que ces interactions de souris s'appliquent à l'ensemble du composant. Lorsque l'utilisateur survole, clique, ou quitte le composant, les fonctions associées (handleMouseEnter, handleMouseLeave, onClick) seront appelées pour exécuter le comportement défini dans ces fonctions.
    >
      {/* Le choix de l'image est basé sur l'état de survol */}
      <img
        src={isHovered ? imageAlt : image} // Au survol on prend l'image orange
        alt={`Logo ${title}`}
        className={`tag-logo tag-logo-${title}`} //comme ça on a un className pour chaque image, avec son nom
      />
      {/* Affichant la légende de l'image */}
      <p>{caption}</p>
    </div>
  );
}
