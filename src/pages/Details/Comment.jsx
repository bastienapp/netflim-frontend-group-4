import "./Comment.css";

export default function Comment() {
  return (
    <div>
      <h2>Commentaires</h2>

      {/* Conteneur pour chaque commentaire utilisateur */}
      <article className="detail-movie-comment-user-container">
        {/* Section pour la photo de profil de l'utilisateur */}
        <div>
          <img
            className="detail-movie-comment-user-photo"
            src="https://thispersondoesnotexist.com/" // L'image change à chaque chargement de la page
            alt="Photo de profil"
          />
        </div>

        {/* Section pour le nom d'utilisateur et le texte du commentaire en dur */}
        <div>
          {/* Nom d'utilisateur en dur */}
          <p className="detail-movie-comment-user-name">
            Utilisateur@XxXdu82XxX
          </p>

          {/* Texte du commentaire en dur */}
          <p className="detail-movie-comment-user-text">
            CHE KOMPREN PAS PK VS ÉMÉ TOUS
          </p>
        </div>
      </article>
    </div>
  );
}
