import React, {useState, useEffect} from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import Synopsis from "../Details/Synopsis";
import Comment from "../Details/Comment";
import "./DetailMovie.css";
import DetailSideBar from "./DetailSideBar";
import PresentationMovie from "./PresentationMovie";
import GoBack from "../../components/GoBack";
import {useParams} from "react-router-dom";

export default function DetailMovie() {
  // Récupération du paramètre 'id' depuis l'URL grâce à useParams
  const {id} = useParams();

  // Déclaration des états pour stocker les données du film individuel et l'état de chargement
  const [individualMovie, setIndividualMovie] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  // Options pour la requête HTTP GET vers l'API TMDB
  const options = {
    method: "GET",
    headers: {
      accept: "application/json",
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzODc4ZWExMTkxOTI3ZGRlNjIzNWY3MzAwYzVkMDY5YiIsInN1YiI6IjY1Nzg1ZGVlODlkOTdmMDBjNjc5MDIyYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nVCJIptl8KEeB1SZpV5GpNMy_WSBiaPwX0kuqgpbXTk",
    },
  };

  // Effet secondaire déclenché lorsque 'id' change, effectuant la requête API
  useEffect(() => {
    fetch(`https://api.themoviedb.org/3/movie/${id}`, options) // Attention pas la même url que sur Home, c'est un url par film selon l'id ajouté
      .then((response) => response.json())
      .then((movie) => {
        // Mise à jour de l'état avec les données du film individuel
        setIndividualMovie(movie);
        // Désactivation de l'état de chargement
        setIsLoading(false);
      })
      .catch((error) => {
        // Gestion des erreurs en cas d'échec de la requête
        console.error("Error fetching data:", error);
        // Désactivation de l'état de chargement
        setIsLoading(false);
      });
  }, [id]); // Réexécute l'effet lorsque le paramètre 'id' change pour récupérer les données du film mises à jour

  // Affichage d'un message de chargement si les données ne sont pas encore disponibles
  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="detail-movie-wrapper">
      <header>
        <Header />
      </header>
      <div className="detail-movie-content detail-movie-content-background">
        {/* Affichage du composant GoBack permettant de revenir en arrière */}
        <GoBack />
        <section className="detail-movie-info">
          {/* Affichage du composant PresentationMovie avec les données du film */}
          <PresentationMovie
            image={individualMovie?.poster_path}
            title={individualMovie?.title}
            rating={individualMovie?.vote_average}
            year={individualMovie?.release_date?.slice(0, 4)}
            tags={individualMovie?.genres} // Attention, tags est un tableau de plusieurs genres
            duration={individualMovie?.runtime}
          />
        </section>
        <div className="detail-movie-down">
          {/* Affichage du composant DetailSideBar avec l'identifiant du film en tant que paramètre */}
          <DetailSideBar movieId={individualMovie?.id} />
          <section className="detail-movie-content-text">
            {/* Affichage du composant Synopsis avec le résumé du film */}
            <Synopsis synopsis={individualMovie?.overview} />
            <Comment />
          </section>
        </div>
        <ButtonMoveToTop />
      </div>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}
