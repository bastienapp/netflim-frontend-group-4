import MainSideBar from "../../components/MainSideBar";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import { useState, useEffect } from "react";
import MovieCard from "../../components/MovieCard";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import "../Home/HomePage.css";
import { loadMovies } from "../../services/api";

export default function ToWatch() {
  const [movieToWatch, setMovieToWatch] = useState([]);
  const [movieListApi, setMovieListApi] = useState([]);

  useEffect(() => {
    async function callApi() {
      const callMovieList = await loadMovies();
      setMovieListApi(callMovieList);

      const localStorageData = { ...localStorage };
      let filmsToWatch = []; // tableau vide

      for (const key in localStorageData) {
        if (key.startsWith("to-watch") && localStorageData[key] === "true") {
          // Vérifie qu'on ne récupère que les films true de la liste to watch
          let newKey = key.split(" ");
          newKey = newKey[1]; // garde le deuxieme élément du tableau

          for (const eachMovie of callMovieList) {
            if (eachMovie.id == newKey) {
              filmsToWatch.push(eachMovie); // pousser les clés dont la valeur est à vraie dans le tableau vide
            }
          }
        }
      }

      setMovieToWatch(filmsToWatch);
    }
    callApi();
  }, []);

  console.log("Liste à regarder : ",movieToWatch)

  return (
    <div className="wrapper">
      <header>
        <Header />
      </header>

      <aside>
        <MainSideBar />
      </aside>
      <div className="content">
        <section className="container-movies">
          {/* 
        parcourir les genres catégorie par catégorie --> boucle
        .filter liste films par catégorie ( genre contenu dans les id_genres du film )
        pour cahque catégorie .map liste films
        */}
          {movieToWatch.map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              duration={current.runtime}
              key={current.id}
              id={current.id}
            />
          ))}
        </section>
        <ButtonMoveToTop />
      </div>

      <footer>
        <Footer />
      </footer>
    </div>
  );
}
