import MainSideBar from "../../components/MainSideBar";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import { useState, useEffect } from "react";
import MovieCard from "../../components/MovieCard";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import "../Home/HomePage.css";
import { loadMovies } from "../../services/api";

export default function FavouriteList() {
  const [movieToFavourite, setMovieToFavourite] = useState([]);
  const [movieListApi, setMovieListApi] = useState([]);

  useEffect(() => {
    async function callApi() {
      const callMovieList = await loadMovies();
      setMovieListApi(callMovieList);

      const localStorageData = { ...localStorage }; // récupère tous les éléments du locatStorage et ça en fait un objet
      let filmsToFavourite = []; // tableau vide

      for (const key in localStorageData) {
        if (key.startsWith("favourites") && localStorageData[key] === "true") {
          // [key] c'est la valeur de Key dans le localStorage. Et key c'est le nom.
          // Vérifie qu'on ne récupère que les films true de la liste favourites (préférés)
          let newKey = key.split(" ");
          newKey = newKey[1]; // garde le deuxieme élément du tableau

          for (const eachMovie of callMovieList) {
            if (eachMovie.id == newKey) {
              filmsToFavourite.push(eachMovie); // pousser les clés dont la valeur est à vraie dans le tableau vide
            }
          }
        }
      }

      setMovieToFavourite(filmsToFavourite); // met à jour la valeur de la variable d'état filmArray avec le tableau filmsToWatch
    }
    callApi()
  }, []);

  return (
    <div className="wrapper">
      <header>
        <Header />
      </header>

      <aside>
        <MainSideBar />
      </aside>
      <div className="content">
        <section className="container-movies">
          {movieToFavourite.map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              duration={current.runtime}
              key={current.id}
              id={current.id}
            />
          ))}
        </section>
        <ButtonMoveToTop />
      </div>

      <footer>
        <Footer />
      </footer>
    </div>
  );
}
