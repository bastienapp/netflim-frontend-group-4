import MainSideBar from "../../components/MainSideBar";
import ButtonMoveToTop from "../../components/ButtonMoveToTop";
import { useState, useEffect } from "react";
import MovieCard from "../../components/MovieCard";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import "../Home/HomePage.css";
import { loadMovies } from "../../services/api";

export default function SeenList() {
  const [movieToWatched, setMovieToWatched] = useState([]);
  const [movieListApi, setMovieListApi] = useState([]);

  useEffect(() => {
    async function callApi() {
      const callMovieList = await loadMovies();
      setMovieListApi(callMovieList);

      const localStorageData = { ...localStorage };
      let filmsToWatched = []; // tableau vide

      for (const key in localStorageData) {
        if (key.startsWith("watched") && localStorageData[key] === "true") {
          // Vérifie qu'on ne récupère que les films true de la liste watched (vus)
          let newKey = key.split(" ");
          newKey = newKey[1]; // garde le deuxieme élément du tableau

          for (const eachMovie of callMovieList) {
            if (eachMovie.id == newKey) {
              filmsToWatched.push(eachMovie); // pousser les clés dont la valeur est à vraie dans le tableau vide
            }
          }
        }
      }

      setMovieToWatched(filmsToWatched); // met à jour la valeur de la variable d'état filmArray avec le tableau filmsToWatch}
    }
    callApi();
  }, []);

  return (
    <div className="wrapper">
      <header>
        <Header />
      </header>

      <aside>
        <MainSideBar />
      </aside>
      <div className="content">
        <section className="container-movies">
          {movieToWatched.map((current) => (
            <MovieCard
              title={current.title}
              poster={current.poster_path}
              duration={current.runtime}
              key={current.id}
              id={current.id}
            />
          ))}
        </section>
        <ButtonMoveToTop />
      </div>

      <footer>
        <Footer />
      </footer>
    </div>
  );
}
