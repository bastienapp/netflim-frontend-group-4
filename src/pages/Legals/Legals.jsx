import React from "react";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import "./Legals.css";
import GoBack from "../../components/GoBack";

function Legal() {
  return (
    <div>
      <header>
        <Header />
      </header>
      <div className="legal-container">
        <GoBack />
        <h1 className="legal-title">Mentions légales</h1>

        <p className="legal-paragraph">
          Conformément aux dispositions des Articles 6-III et 19 de la Loi
          n°2004-575 du 21 juin 2004 pour la Confiance dans l'économie
          numérique, dite L.C.E.N., il est porté à la connaissance des
          utilisateurs et visiteurs du site Netflim les présentes mentions
          légales.
        </p>

        <h2 className="legal-section-title">Édition du site</h2>

        <p className="legal-paragraph">
          Le site Netflim est édité par la société Netflim SAS au capital de 1
          000 000 euros, immatriculée au RCS de Paris sous le numéro 123 456
          789, dont le siège social est situé au 123 Rue de la République, 75001
          Paris.
        </p>

        <h2 className="legal-section-title">Directeur de la publication</h2>

        <p className="legal-paragraph">Monsieur Jean Dupont.</p>

        <h2 className="legal-section-title">Contact</h2>

        <p className="legal-paragraph">
          Vous pouvez nous contacter par email à l'adresse :{" "}
          <a className="legal-email" href="mailto:contact@netflim.com">
            contact@netflim.com
          </a>
        </p>

        <h2 className="legal-section-title">Hébergement</h2>

        <p className="legal-paragraph">
          L'hébergement du site est assuré par la société Hébergement SAS, dont
          le siège social est situé au 456 Avenue de l'Hébergement, 75002 Paris.
        </p>

        {/* Ajoutez d'autres sections au besoin */}
      </div>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default Legal;
